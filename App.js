import React, { Component } from 'react';
import { Text, View, StyleSheet, Button} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import Timer from './timer';


export default class TemplateApp extends Component {
  constructor() {
    super()
    this.initialTime = 25 * 60
    this.state = { currentTime: this.initialTime }
  }
  
  render() {
    return (
      <>
        <View style={styles.body}>
          <Text style={styles.text}>Pomodoro</Text>
          <Timer 
            currentTime={this.state.currentTime}
          />
          <Button 
            title="Start"
            onPress={() => this.currentTime()}
          />
          <Button
            title="Reset"
            onPress={() => this.reset()}
          />
        </View>
      </>
  )}

  currentTime() {
    this.timer = setInterval(
      () => this.setState((state) => ({currentTime: state.currentTime - 1 })),
      1000
    )
  }

  reset() {
    clearInterval(this.timer)
    this.setState({currentTime: this.initialTime})
  }
}

const styles = StyleSheet.create({
  body: {
    backgroundColor: Colors.white,
  },
  text: {
    fontSize: 50,
    fontWeight: '600',
    padding: 60,
    textAlign: 'center',
  },
});
