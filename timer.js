import React, { Component } from 'react';
import { Text, StyleSheet, View} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export default class Timer extends Component {
  constructor() {
    super()
  }

  render() {
    return (
      <>
        <View style={styles.body}>
          <Text style={styles.minutes}>{this.format(this.props.currentTime)}</Text>
        </View>
      </>
    )
  }

  format(data) {
    const formater = new Intl.NumberFormat('es-ES', {minimumIntegerDigits: 2})
    const minutes = parseInt(data / 60)
    const seconds = parseInt(data % 60)

    return `${formater.format(minutes)} : ${formater.format(seconds)}`
  }
}

const styles = StyleSheet.create({
  body: {
    backgroundColor: Colors.white,
    textAlign: 'center',
    marginBottom: 20
  },
  minutes: {
    fontSize: 42,
    textAlign: 'center'
  }
})